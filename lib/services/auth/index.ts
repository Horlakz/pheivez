import { client } from "../client";
import { Storage } from "../utils";

// initailize the storage class
const storage = new Storage();

// types
interface AuthTypes {
  name?: string;
  email: string;
  password: string;
}

export default class Auth {
  login(data: AuthTypes) {
    return client({
      method: "POST",
      url: "/auth/login/",
      data,
    });
  }

  register(data: AuthTypes) {
    return client({
      method: "POST",
      url: "/auth/register/",
      data,
    });
  }

  forgotPassword(data: AuthTypes) {
    return client({
      method: "POST",
      url: "/auth/forgot-password/",
      data,
    });
  }

  resetPassword(data: AuthTypes) {
    return client({
      method: "PUT",
      url: "/auth/reset-password/",
      data,
    });
  }

  getUser() {
    return client({
      method: "get",
      url: "/auth/user/",
      headers: {
        Authorization: `Bearer ${storage.get("token")}`,
      },
    });
  }
}
