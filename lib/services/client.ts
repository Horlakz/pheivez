import axios, { type AxiosInstance } from "axios";

axios.defaults.baseURL = process.env.API_URL;
axios.defaults.xsrfHeaderName = "X-CSRFToken";
axios.defaults.responseEncoding = "utf8";

export const client: AxiosInstance = axios.create({});
