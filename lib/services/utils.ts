import { setCookies, getCookie, deleteCookie } from "cookies-next";

const DAY_IN_SECONDS = 24 * 60 * 60;

export class Storage {
  set(key: string, value: string) {
    return setCookies(key, value, { maxAge: DAY_IN_SECONDS });
  }

  get(key: string) {
    return getCookie(key);
  }

  delete(key: string) {
    return deleteCookie(key);
  }
}
