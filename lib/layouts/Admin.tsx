import { FC } from "react";
import Head from "next/head";

// component(s)
import Footer from "../components/BaseLayout/Footer";

// types
interface Props {
  children: React.ReactNode;
}

const AdminLayout: FC<Props> = ({ children }) => {
  return (
    <>
      <Head>
        <title>Admin Dashboard || Pheivez Arts</title>
      </Head>
      {children}
      <Footer />
    </>
  );
};

export default AdminLayout;
