import { FC } from "react";
import Head from "next/head";

// component(s)
import Footer from "../components/BaseLayout/Footer";
import Header from "../components/BaseLayout/Header";

interface Props {
  children: React.ReactNode;
}

const BaseLayout: FC<Props> = ({ children }) => {
  return (
    <>
      <Head>
        <title>Pheivez Arts</title>
        {/* add meta tags */}
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta name="description" content="Pheivez Arts Website" />
        <meta
          name="keywords"
          content="Pheivez Arts, Artist, Painting, Art, Pheivez"
        />
        <meta name="author" content="Pheivez" />
        <meta name="robots" content="index, follow" />
        <meta name="googlebot" content="index, follow" />
        <meta name="revisit-after" content="3 days" />
        <meta name="language" content="en" />
      </Head>
      <Header />
      {children}
      <Footer />
    </>
  );
};

export default BaseLayout;
