import type { NextPage } from "next";
import Head from "next/head";
import Link from "next/link";

// layout(s)
import BaseLayout from "../lib/layouts";

const Home: NextPage = () => {
  return (
    <BaseLayout>
      <Head>
        <title>Pheivez Arts</title>
      </Head>
      <p>updates are coming</p>
      <Link href="/admin/login">Login to your Admin Dashboard here</Link>
    </BaseLayout>
  );
};

export default Home;
