import { useState, useEffect } from "react";
import type { NextPage } from "next";
import { useRouter } from "next/router";
import { useQuery } from "react-query";

// layout
import AdminLayout from "../../lib/layouts/Admin";

// service(s)
import Auth from "../../lib/services/auth";
import { Storage } from "../../lib/services/utils";

const AdminDashboard: NextPage = () => {
  // use state
  const [isAuth, setIsAuth] = useState(false);

  // initialize router
  const router = useRouter();

  // initialize service
  const auth = new Auth();
  const storage = new Storage();

  // use query
  const { data, isError, isLoading, isSuccess } = useQuery("user", () =>
    auth.getUser()
  );

  useEffect(() => {
    setIsAuth(storage.get("token") ? true : false);

    if (isAuth) {
      router.push("/admin/login");
    }
  }, []);

  if (isLoading) {
    return (
      <AdminLayout>
        <p>Loading...</p>
      </AdminLayout>
    );
  }

  if (isError) {
    return (
      <AdminLayout>
        <p>Error!</p>
      </AdminLayout>
    );
  }

  if (isSuccess) {
    return (
      <AdminLayout>
        <main className="text-xl text-blue-600 p-10">
          <span>Welcome {data?.data?.name}</span>
          <span>Your email is {data?.data?.email}</span>

          <p>You will be automatically logged out after a day</p>
          <button
            type="submit"
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
            onClick={() => {
              router.push("/admin/logout");
            }}
          >
            Logout
          </button>
        </main>
        ;
      </AdminLayout>
    );
  }
  return <></>;
};

export default AdminDashboard;
