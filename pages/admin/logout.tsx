import { useEffect } from "react";
import type { NextPage } from "next";
import { useRouter } from "next/router";

// service(s)
import { Storage } from "../../lib/services/utils";

const Logout: NextPage = () => {
  // initialize router
  const router = useRouter();

  // initialize service
  const storage = new Storage();

  useEffect(() => {
    storage.delete("token");
    router.push("/admin/login");
  }, []);

  return <div>Logout</div>;
};

export default Logout;
