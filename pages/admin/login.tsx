import { useState } from "react";
import type { NextPage } from "next";
import { useMutation } from "react-query";
import { useRouter } from "next/router";
import { toast } from "react-toastify";

// layout
import AdminLayout from "../../lib/layouts/Admin";

// service(s)
import Auth from "../../lib/services/auth";
import { Storage } from "../../lib/services/utils";

// types
interface AuthTypes {
  email: string;
  password: string;
}

const AdminLogin: NextPage = () => {
  // use state
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  // initialize router
  const router = useRouter();

  // initialize service
  const auth = new Auth();
  const storage = new Storage();

  const login = useMutation(async (data: AuthTypes) => await auth.login(data), {
    onSuccess: (res) => {
      toast.success("Successfully logged in!");
      router.push("/admin");
      storage.set("token", res.data?.token);
    },
    onError: (err: any) => {
      toast.error(err.response.data.message);
    },
  });

  return (
    <AdminLayout>
      <form className="p-20">
        <div>
          <label htmlFor="email">Email</label>
          <input
            type="text"
            id="email"
            className="border border-gray-400 p-2 w-full"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <div>
          <label htmlFor="password">Password</label>
          <input
            type="password"
            id="password"
            className="border border-gray-400 p-2 w-full"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <button
          type="submit"
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
          onClick={(e) => {
            e.preventDefault();
            login.mutate({ email, password });
          }}
        >
          Login
        </button>
      </form>
    </AdminLayout>
  );
};

export default AdminLogin;
