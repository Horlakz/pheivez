import { useState } from "react";
import type { NextPage } from "next";
import { useMutation } from "react-query";
import { useRouter } from "next/router";
import { toast } from "react-toastify";

// layout
import AdminLayout from "../../lib/layouts/Admin";

// service(s)
import Auth from "../../lib/services/auth";
import { Storage } from "../../lib/services/utils";

interface AuthTypes {
  name: string;
  email: string;
  password: string;
}

const AdminRegister: NextPage = () => {
  // use state
  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  // initialize router
  const router = useRouter();

  // initialize service
  const auth = new Auth();
  const storage = new Storage();

  const register = useMutation(async (data: AuthTypes) => await auth.register(data), {
    onSuccess: (res) => {
      toast.success("Successfully registered!");
      router.push("/admin");
      storage.set("token", res.data?.token);
    },
    onError: (err: any) => {
      toast.error(err.response.data.message);
    },
  });

  return (
    <AdminLayout>
      <form className="p-20">
        <div>
          <label htmlFor="firstName">First Name</label>
          <input
            type="text"
            id="firstName"
            className="border border-gray-400 p-2 w-full"
            value={firstname}
            onChange={(e) => setFirstname(e.target.value)}
          />
        </div>
        <div>
          <label htmlFor="lastName">Last Name</label>
          <input
            type="text"
            id="lastName"
            className="border border-gray-400 p-2 w-full"
            value={lastname}
            onChange={(e) => setLastname(e.target.value)}
          />
        </div>
        <div>
          <label htmlFor="email">Email</label>
          <input
            type="email"
            id="email"
            className="border border-gray-400 p-2 w-full"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <div>
          <label htmlFor="password">Password</label>
          <input
            type="password"
            id="password"
            className="border border-gray-400 p-2 w-full"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <button
          type="submit"
          onClick={(e) => {
            e.preventDefault();
            register.mutate({
              name: `${firstname} ${lastname}`,
              email,
              password,
            });
          }}
          className="px-4 py-2 bg-gray-800 text-white"
        >
          Register
        </button>
      </form>
    </AdminLayout>
  );
};

export default AdminRegister;
